import 'package:flutter/material.dart';
import 'package:test_project/features/movie_feature/domain/entities/movie.dart';
import 'package:test_project/features/movie_feature/presentation/pages/home/movie_page.dart';
import 'package:test_project/features/movie_feature/presentation/pages/movie_detail/movie_detail.dart';

class AppRoutes {
  static Route onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return _materialRoute(const Movies());

      case '/movie_details':
        return _materialRoute(
          MovieDetailsView(movie: settings.arguments as MovieEntity),
        );

      default:
        return _materialRoute(const Movies());
    }
  }

  static Route<dynamic> _materialRoute(Widget page) {
    return MaterialPageRoute(builder: (_) => page);
  }
}

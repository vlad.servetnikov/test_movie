import 'package:test_project/core/resources/data_state.dart';
import 'package:test_project/features/movie_feature/domain/entities/movie.dart';

abstract class MovieRepository {
  Future<DataState<List<MovieEntity>>> getMovie(String? query);
}

class MovieQueryParams {
  final String? query;

  MovieQueryParams({this.query});
}
import 'package:test_project/core/resources/data_state.dart';
import 'package:test_project/core/usecase/usecase.dart';
import 'package:test_project/features/movie_feature/domain/entities/movie.dart';
import 'package:test_project/features/movie_feature/domain/repository/movie_repository.dart';
import 'package:test_project/features/movie_feature/domain/usecases/movie_query_params.dart';

class GetMovieUseCase
    implements UseCase<DataState<List<MovieEntity>>, MovieQueryParams> {
  final MovieRepository _movieRepository;

  GetMovieUseCase(this._movieRepository);

  @override
  Future<DataState<List<MovieEntity>>> call({MovieQueryParams? params}) {
    String? query = params?.query;

    return _movieRepository.getMovie(query);
  }
}

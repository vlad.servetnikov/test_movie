import 'dart:io';

import 'package:dio/dio.dart';
import 'package:test_project/core/constants/constants.dart';
import 'package:test_project/core/resources/data_state.dart';
import 'package:test_project/features/movie_feature/data/data_sources/local/app_database.dart';
import 'package:test_project/features/movie_feature/data/data_sources/remote/movie_api_service.dart';
import 'package:test_project/features/movie_feature/data/models/movie.dart';
import 'package:test_project/features/movie_feature/domain/repository/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieApiService _movieApiService;
  final AppDatabase _appDatabase;

  MovieRepositoryImpl(
    this._movieApiService,
    this._appDatabase,
  );

  @override
  Future<DataState<List<MovieModel>>> getMovie(String? query) async {
    try {
      final savedMovies = await _appDatabase.articleDao.getMovies();
      final httpResponse = await _movieApiService.getMovies(apiKey, query);

      if (savedMovies.isNotEmpty && query == null) {
        return DataSuccess(savedMovies);
      }

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        List<MovieModel> movies = httpResponse.data.results!;

        await _appDatabase.articleDao.deleteAllMovies();

        for (var movie in movies) {
          await _appDatabase.articleDao.insertMovie(
            MovieModel.fromEntity(movie),
          );
        }

        return DataSuccess(movies);
      } else {
        return DataFailed(
          DioException(
            requestOptions: httpResponse.response.requestOptions,
            response: httpResponse.response,
            error: httpResponse.response.statusMessage,
            type: DioExceptionType.badResponse,
          ),
        );
      }
    } on DioException catch (e) {
      return DataFailed(e);
    }
  }
}

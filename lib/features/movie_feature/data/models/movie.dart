import 'package:floor/floor.dart';
import 'package:test_project/features/movie_feature/domain/entities/movie.dart';

class MovieData {
  List<MovieModel>? results;

  MovieData({this.results});

  MovieData.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      results =
          (json['results'] as List).map((v) => MovieModel.fromJson(v)).toList();
    }
  }
}

@Entity(tableName: 'movies', primaryKeys: ['id'])
class MovieModel extends MovieEntity {
  const MovieModel({
    final bool? adult,
    String? backdropPath,
    int? id,
    String? originalLanguage,
    String? originalTitle,
    String? overview,
    double? popularity,
    String? posterPath,
    String? releaseDate,
    String? title,
    bool? video,
    double? voteAverage,
    int? voteCount,
  }) : super(
          backdropPath: backdropPath,
          id: id,
          originalLanguage: originalLanguage,
          originalTitle: originalTitle,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          releaseDate: posterPath,
          title: title,
          video: video,
          voteAverage: voteAverage,
          voteCount: voteCount,
        );

  factory MovieModel.fromJson(Map<String, dynamic> map) {
    return MovieModel(
      adult: map['adult'],
      backdropPath: map['backdrop_path'],
      id: map['id'],
      originalLanguage: map['original_language'],
      originalTitle: map['original_title'],
      overview: map['overview'],
      popularity: map['popularity'],
      posterPath: map['poster_path'],
      releaseDate: map['release_date'],
      title: map['title'],
      video: map['video'],
      voteAverage: map['vote_average'],
      voteCount: map['vote_count'],
    );
  }

  factory MovieModel.fromEntity(MovieEntity entity) {
    return MovieModel(
      adult: entity.adult,
      backdropPath: entity.backdropPath,
      id: entity.id,
      originalLanguage: entity.originalLanguage,
      originalTitle: entity.originalTitle,
      overview: entity.overview,
      popularity: entity.popularity,
      posterPath: entity.posterPath,
      releaseDate: entity.releaseDate,
      title: entity.title,
      video: entity.video,
      voteAverage: entity.voteAverage,
      voteCount: entity.voteCount,
    );
  }
}

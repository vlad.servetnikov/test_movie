import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:test_project/features/movie_feature/data/data_sources/local/DAO/movie_dao.dart';
import 'dart:async';

import 'package:test_project/features/movie_feature/data/models/movie.dart';

part 'app_database.g.dart';

@Database(version: 1, entities: [MovieModel])
abstract class AppDatabase extends FloorDatabase {
  MovieDao get articleDao;
}

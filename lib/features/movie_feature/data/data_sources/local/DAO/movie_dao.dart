import 'package:floor/floor.dart';
import 'package:test_project/features/movie_feature/data/models/movie.dart';

@dao
abstract class MovieDao {
  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertMovie(MovieModel movie);

  @Query('SELECT * FROM movies')
  Future<List<MovieModel>> getMovies();

  @Query('DELETE FROM movies')
  Future<void> deleteAllMovies();
}

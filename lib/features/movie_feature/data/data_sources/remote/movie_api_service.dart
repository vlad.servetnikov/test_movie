import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';
import 'package:test_project/core/constants/constants.dart';
import 'package:test_project/features/movie_feature/data/models/movie.dart';

part 'movie_api_service.g.dart';

@RestApi(baseUrl: newsAPIBaseURL)
abstract class MovieApiService {
  factory MovieApiService(Dio dio) = _MovieApiService;

  @GET('/search/movie')
  Future<HttpResponse<MovieData>> getMovies(
    @Query("api_key") String apiKey,
    @Query("query") String? query,
  );
}

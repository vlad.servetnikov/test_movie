import 'package:flutter/material.dart';
import 'package:test_project/features/movie_feature/presentation/widgets/image_widget.dart';
import '../../../domain/entities/movie.dart';

class MovieDetailsView extends StatelessWidget {
  final MovieEntity? movie;

  const MovieDetailsView({Key? key, this.movie}) : super(key: key);

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildMovieTitleAndDate(),
          _buildMovieImage(),
          _buildMovieDescription(),
        ],
      ),
    );
  }

  Widget _buildMovieTitleAndDate() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 22),
      child: Text(
        movie?.title ?? '',
        style: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w900,
        ),
      ),
    );
  }

  Widget _buildMovieImage() {
    return Container(
      width: double.maxFinite,
      height: 450,
      margin: const EdgeInsets.only(top: 14),
      child: ImageWidget(
        image: movie?.posterPath,
        radius: 0,
      ),
    );
  }

  Widget _buildMovieDescription() {
    if (movie?.popularity == null) return const SizedBox();

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 12),
          Row(
            children: [
              const Icon(Icons.star, color: Colors.amber),
              Text(
                '${movie!.popularity}',
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ],
          ),
          const SizedBox(height: 12),
          Text(
            movie!.originalTitle ?? '',
            style: const TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }

  void _onBackButtonTapped(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (context) => GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => _onBackButtonTapped(context),
            child: const Icon(Icons.arrow_back_ios, color: Colors.black),
          ),
        ),
      ),
      body: _buildBody(),
    );
  }
}

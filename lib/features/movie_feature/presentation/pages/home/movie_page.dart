import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_bloc.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_event.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_state.dart';
import 'package:test_project/features/movie_feature/presentation/widgets/image_widget.dart';
import '../../../domain/entities/movie.dart';

class Movies extends StatelessWidget {
  const Movies({Key? key}) : super(key: key);

  void _onMoviePressed(BuildContext context, MovieEntity article) {
    Navigator.pushNamed(context, '/movie_details', arguments: article);
  }

  void _onSearchChanged(BuildContext context, String query) {
    context.read<RemoteMovieBloc>().add(MovieSearchUpdated(query));
  }

  Widget _buildTable(BuildContext context, List<MovieEntity> movies) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        showCheckboxColumn: false,
        columns: const [
          DataColumn(
            label: Text('Image'),
          ),
          DataColumn(
            label: Text('Name'),
          ),
          DataColumn(
            label: Text('Description'),
          ),
        ],
        rows: movies
            .map(
              (rows) => DataRow(
                onSelectChanged: (newValue) => _onMoviePressed(context, rows),
                cells: [
                  DataCell(
                    ImageWidget(image: rows.posterPath),
                  ),
                  DataCell(Text(
                    rows.title ?? '',
                  )),
                  DataCell(Text(rows.originalTitle ?? '')),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Movies',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: TextField(
                  onChanged: (value) => _onSearchChanged(context, value),
                  decoration: const InputDecoration(
                    labelText: 'Search Movies',
                  ),
                ),
              ),
              BlocBuilder<RemoteMovieBloc, RemoteMovieState>(
                builder: (_, state) {
                  if (state is RemoteMoviesLoading) {
                    return const CupertinoActivityIndicator();
                  }
                  if (state is RemoteMoviesEmpty) {
                    return const Text('No movies');
                  }

                  if (state is RemoteMoviesError) {
                    return const Center(child: Icon(Icons.refresh));
                  }
                  if (state is RemoteMoviesDone) {
                    return _buildTable(context, state.movies!);
                  }
                  return const SizedBox();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

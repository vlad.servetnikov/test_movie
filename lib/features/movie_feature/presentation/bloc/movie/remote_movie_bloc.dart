import 'package:test_project/core/resources/data_state.dart';
import 'package:test_project/features/movie_feature/domain/usecases/get_movie.dart';
import 'package:test_project/features/movie_feature/domain/usecases/movie_query_params.dart';
import 'package:test_project/utils/debouncer.dart';

import 'remote_movie_event.dart';
import 'remote_movie_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RemoteMovieBloc extends Bloc<RemoteMovieEvent, RemoteMovieState> {
  final GetMovieUseCase _getMovieUseCase;
  final _debouncer = Debouncer(milliseconds: 500);

  RemoteMovieBloc(this._getMovieUseCase) : super(const RemoteMoviesLoading()) {
    on<GetMovie>(onGetMovie);
    on<MovieSearchUpdated>(_onSearchUpdated);
  }

  Future<void> onGetMovie(
    GetMovie event,
    Emitter<RemoteMovieState> emit,
  ) async {
    final dataState = await _getMovieUseCase(
      params: MovieQueryParams(query: event.query),
    );

    if (dataState is DataSuccess) {
      if (dataState.data!.isNotEmpty) {
        emit(RemoteMoviesDone(articles: dataState.data ?? []));
      } else {
        emit(const RemoteMoviesEmpty());
      }
    }

    if (dataState is DataFailed) {
      emit(RemoteMoviesError(error: dataState.error!));
    }
  }

  void _onSearchUpdated(
    MovieSearchUpdated event,
    Emitter<RemoteMovieState> emit,
  ) {
    if (event.query == null || event.query!.isEmpty) return;

    _debouncer.run(() {
      if (event.query!.length > 2) {
        add(GetMovie(query: event.query));
      }
    });
  }
}

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:test_project/features/movie_feature/domain/entities/movie.dart';

abstract class RemoteMovieState extends Equatable {
  final List<MovieEntity>? movies;
  final DioException? error;

  const RemoteMovieState({this.movies, this.error});

  @override
  List<Object?> get props => [movies, error];
}

class RemoteMoviesLoading extends RemoteMovieState {
  const RemoteMoviesLoading();
}

class RemoteMoviesEmpty extends RemoteMovieState {
  const RemoteMoviesEmpty();
}

class RemoteMoviesDone extends RemoteMovieState {
  const RemoteMoviesDone({required List<MovieEntity> articles})
      : super(movies: articles);
}

class RemoteMoviesError extends RemoteMovieState {
  const RemoteMoviesError({required DioException error}) : super(error: error);
}

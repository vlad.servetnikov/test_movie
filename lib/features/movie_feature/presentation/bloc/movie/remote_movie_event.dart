abstract class RemoteMovieEvent {
  const RemoteMovieEvent();
}

class GetMovie extends RemoteMovieEvent {
  final String? query;

  GetMovie({this.query});
}

class MovieSearchUpdated extends RemoteMovieEvent {
  final String? query;

  MovieSearchUpdated(this.query);
}

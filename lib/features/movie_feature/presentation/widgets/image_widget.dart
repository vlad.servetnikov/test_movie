import 'package:flutter/material.dart';
import 'package:test_project/core/constants/constants.dart';

class ImageWidget extends StatelessWidget {
  final String? image;
  final double width;
  final double height;
  final double radius;

  const ImageWidget({
    super.key,
    this.image,
    this.width = 40,
    this.height = 40,
    this.radius = 50,
  });

  String get _getImage {
    if (image == null) return kDefaultImage;

    return '$imageHost$image';
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: Image.network(
        _getImage,
        width: width,
        height: height,
        fit: BoxFit.cover,
        loadingBuilder: (BuildContext context, Widget child,
            ImageChunkEvent? loadingProgress) {
          if (loadingProgress == null) return child;
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 1.2,
              value: loadingProgress.expectedTotalBytes != null
                  ? loadingProgress.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes!
                  : null,
            ),
          );
        },
      ),
    );
  }
}

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:test_project/features/movie_feature/data/data_sources/local/app_database.dart';
import 'package:test_project/features/movie_feature/data/data_sources/remote/movie_api_service.dart';
import 'package:test_project/features/movie_feature/data/repository/movie_repository_impl.dart';
import 'package:test_project/features/movie_feature/domain/repository/movie_repository.dart';
import 'package:test_project/features/movie_feature/domain/usecases/get_movie.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_bloc.dart';

final locator = GetIt.instance;

Future<void> initializeDependencies() async {
  final database =
      await $FloorAppDatabase.databaseBuilder('app_database.db').build();
  locator.registerSingleton<AppDatabase>(database);
  // Dio
  locator.registerSingleton<Dio>(Dio());

  // Data sources
  locator.registerSingleton<MovieApiService>(MovieApiService(locator()));

  // Repository
  locator.registerLazySingleton<MovieRepository>(
      () => MovieRepositoryImpl(locator(), locator()));

  // Use cases
  locator.registerLazySingleton(() => GetMovieUseCase(locator()));

  // Blocs
  locator.registerFactory(() => RemoteMovieBloc(locator()));
}

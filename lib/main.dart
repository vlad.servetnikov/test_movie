import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/config/routes/routes.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_bloc.dart';
import 'package:test_project/features/movie_feature/presentation/bloc/movie/remote_movie_event.dart';
import 'package:test_project/features/movie_feature/presentation/pages/home/movie_page.dart';

import 'injection_container.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RemoteMovieBloc>(
      create: (context) => locator()..add(GetMovie()),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: AppRoutes.onGenerateRoutes,
        home: Movies(),
      ),
    );
  }
}
